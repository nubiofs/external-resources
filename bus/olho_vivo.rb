require 'rest-client'
require 'colorize'
require 'json'

class OlhoVivo
  attr_accessor :token
  def initialize(version = 'v2.1')
    project_path = File.dirname(__FILE__)
    config = YAML.load_file("#{project_path}/settings.yml")
    @token = config["API_TOKEN"]
    @version = version
    @cookies = auth
  end
  
  def auth
    # token API olho vivo
    url = "http://api.olhovivo.sptrans.com.br/#{@version}/Login/Autenticar?token=#{@token}"
    response = RestClient.post(url, {})
    @cookies = response.cookies['apiCredentials']
  end
  
  def get_trip(query)
    list = JSON.parse(RestClient.get("http://api.olhovivo.sptrans.com.br/#{@version}/Linha/Buscar?termosBusca=#{query}", {cookies: {"apiCredentials": @cookies}}).body)
  rescue Exception => e
    puts "Could not get buses positions:".bold.red
    puts "\t#{e.message}".red
    return nil
  end

  def get_positions
    list = nil
    begin
      list = JSON.parse(RestClient.get("http://api.olhovivo.sptrans.com.br/#{@version}/Posicao", {cookies: {"apiCredentials": @cookies}}).body)
    rescue RestClient::MethodNotAllowed => e
      auth
      list = JSON.parse(RestClient.get("http://api.olhovivo.sptrans.com.br/#{@version}/Posicao", {cookies: {"apiCredentials": @cookies}}).body)
    rescue Exception => e
      puts "Could not get buses positions from line #{line}".red.bold
      puts "\t#{e.message}"
    end
    return list
  end
end
